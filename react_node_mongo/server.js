const express = require('express');

const app = express();

app.get('/api/customers', (req, res) => {
  const customers = [
    {id: 1, firstName: 'Piyush ', lastName: 'Bajpai'},
    {id: 2, firstName: 'Anubhav', lastName: 'Singh'},
    {id: 3, firstName: 'Rishabh', lastName: 'Verma'},
  ];

  res.json(customers);
});

const port = 5000;

app.listen(port, () => `Server running on port ${port}`);